package com.example.jquery;

import android.app.Application;

import com.example.jquery.utils.AssetsFindFiles;
import com.example.jquery.utils.ParseXML;

public class App extends Application {
    private int filesCount;

    private String[] filesPath;
    private String[] filesTitle;
    private boolean empty;

    @Override
    public void onCreate() {
        super.onCreate();
        AssetsFindFiles assetsFindFiles = new AssetsFindFiles(getApplicationContext().getAssets(), "db");
        filesCount = assetsFindFiles.getFilesCount();
        filesTitle = new String[filesCount];
        filesPath = new String[filesCount];
        if (filesCount == 0) {
            empty = true;
            return;
        } else {
            empty = false;
        }
        for (int i = 0; i < filesCount; i++) {
            filesTitle[i] = ParseXML.getXMLTagValue(
                    assetsFindFiles.getFileInputStream(i),
                    "title"
            );
            filesPath[i] = assetsFindFiles.getFileWebPath(i);
        }
    }

    public String[] getFilesTitle() {
        if (empty) {
            return new String[]{};
        }
        return filesTitle;
    }

    public String getFilePath(int number) {
        if (empty) {
            return null;
        }
        return filesPath[number];
    }

    public boolean isEmpty() {
        return empty;
    }
}

