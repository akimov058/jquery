package com.example.jquery.utils;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

public class AssetsFindFiles {

    private final static String WEB_PATH = "file:///android_asset";

    private AssetManager asset;
    private String[] filesName;
    private String dir;

    public AssetsFindFiles(AssetManager asset, String dir) {
        this.asset = asset;
        this.dir = dir;
        try {
            filesName = asset.list(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getFilesCount() {
        return filesName.length;
    }

    public String[] getFilesName() {
        return filesName;
    }

    public String getFileWebPath(int number) {
        if (number > filesName.length) {
            return null;
        }
        return String.format(
                "%s/%s/%s",
                WEB_PATH,
                dir,
                filesName[number]
        );
    }

    public InputStream getFileInputStream(int number) {
        try {
            return asset.open(String.format(
                    "%s/%s",
                    dir,
                    filesName[number]
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
